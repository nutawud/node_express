const db = require("../models");
const User = db.user;
const jwt = require('jsonwebtoken');
const config = require("../config/auth.config");

exports.allAccess = (req, res) => {
  User.findAll({ limit: 10 }).then(userAll => {
    res.status(200).send({
      status: 'success',
      users: userAll
    });
  })

};

exports.userBoard = (req, res) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    }).catch(err => {
      res.status(500).send({ message: err.message });
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        status: "error",
        data: "Unauthorized!"
      });
    }
    User.findOne({
      where: {
        id: decoded.id
      },
      attributes: {
        exclude: ['password', 'createdAt', 'updatedAt']
      }
    }).then(userData => {
      res.status(200).send({
        status: 'success',
        data: userData
      })
    }).catch(err => {
      res.status(500).send({ message: err.message });
    });
  });
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};
